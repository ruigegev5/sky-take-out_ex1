package com.sky.service.impl;

import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WorkspaceService workspaceService;

    /**
     * 营业额统计接口
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {
        //1.创建一个集合来接收对象
        List<LocalDate> dateList = new ArrayList<>();
        //2.先把第一个日期添加进去
        dateList.add(begin);
        //3.循环遍历一下 当begin不等于end的时候 就可以一直+1
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }
        String join = StringUtils.join(dateList, ",");

        //select sum(*) from orders where status = 5 and orderTime >= begin and orderTime <= end
        //查询状态为已完成的订单
        ArrayList<Double> turnoverList = new ArrayList<>();
        dateList.stream().forEach(localDate -> {
            LocalDateTime beginTime = LocalDateTime.of(localDate, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(localDate, LocalTime.MAX);
            Integer status = Orders.COMPLETED;
            Map<String,Object> map = new HashMap<>();
            map.put("beginTime",beginTime);
            map.put("endTime",endTime);
            map.put("status",status);
            Double amount = ordersMapper.countTurnover(map);
            amount = amount == null ? 0.0 : amount;
            turnoverList.add(amount);
        });
        String join1 = StringUtils.join(turnoverList, ",");

        //4.使用工具类来进行集合中每个元素的分隔符
        return TurnoverReportVO
                .builder()
                .dateList(join)
                .turnoverList(join1)
                .build();
    }

    /**
     * 用户统计接口
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {
        List<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }
        String joinDate = StringUtils.join(dateList, ",");
        //新增用户数
        List<Integer> newUserList = new ArrayList<>();
        //总用户数
        List<Integer> totalUserList = new ArrayList<>();
        for (LocalDate localDate : dateList) {
            //开始时间
            LocalDateTime beginTime = LocalDateTime.of(localDate, LocalTime.MIN);
            //结束时间
            LocalDateTime endTime = LocalDateTime.of(localDate, LocalTime.MAX);
            //总用户 select * from users where create_time <= ?
            Integer totalUser = getuserCount(null, endTime);
            //新用户
//            map.put("begin",beginTime);
//            Integer newUser = userMapper.getTotalUser(map);
            Integer newUser = getuserCount(beginTime, endTime);
            newUserList.add(newUser);
            totalUserList.add(totalUser);
        }
        return UserReportVO
                .builder()
                .dateList(joinDate)
                .newUserList(StringUtils.join(newUserList, ","))
                .totalUserList(StringUtils.join(totalUserList, ","))
                .build();
    }

    /**
     * 订单统计接口
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public OrderReportVO ordersStatistics(LocalDate begin, LocalDate end) {
        List<LocalDate> dateList = new ArrayList<>();
        //获取日期区间
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }

        //创建订单总数集合和有效订单总数集合
        List<Integer> orderCountList = new ArrayList<>();
        List<Integer> validOrderCountList = new ArrayList<>();
/*        Integer orderCountSum = 0;
        Integer validOrderCountSum = 0;*/
        for (LocalDate date : dateList) {
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            //每天的订单数
            Integer orderCount = getByOrderCountAndValidOrderCount(beginTime, endTime, null);
            //每天的有效订单数
            Integer validOrderCount = getByOrderCountAndValidOrderCount(beginTime, endTime, Orders.COMPLETED);
            orderCountList.add(orderCount);
            validOrderCountList.add(validOrderCount);
/*            orderCountSum += orderCount;
            validOrderCountSum += validOrderCount;*/
        }

        //订单总数
        Integer orderCountSum = orderCountList.stream().reduce(Integer::sum).get();
        //有效订单总数
        Integer validCountSum = validOrderCountList.stream().reduce(Integer::sum).get();
        //订单完成率 有效订单书/订单总数*100%
        Double orderCompletionRate = 0.0;
        if (orderCountSum != 0.0) {
//            orderCompletionRate = validCountSum * 1.0 / orderCountSum;
            orderCompletionRate = validCountSum.doubleValue() / orderCountSum;
        }

        return OrderReportVO
                .builder()
                .dateList(StringUtils.join(dateList, ","))
                .orderCompletionRate(orderCompletionRate)
                .orderCountList(StringUtils.join(orderCountList, ","))
                .totalOrderCount(orderCountSum)
                .validOrderCount(validCountSum)
                .validOrderCountList(StringUtils.join(validOrderCountList, ","))
                .build();
    }

    /**
     * 查询销量排名top10接口
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO top10(LocalDate begin, LocalDate end) {
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        Map<String, Object> map = new HashMap<>();
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        map.put("status", Orders.COMPLETED);
        //写sql语句
        List<GoodsSalesDTO> top10List = ordersMapper.top10(map);
        List<String> nameList = top10List.stream().map(GoodsSalesDTO::getName).collect(Collectors.toList());
        List<Integer> numberList = top10List.stream().map(GoodsSalesDTO::getNumber).collect(Collectors.toList());
        return SalesTop10ReportVO.builder()
                .nameList(StringUtils.join(nameList, ","))
                .numberList(StringUtils.join(numberList, ","))
                .build();
    }

    /**
     * 导出Excel报表接口
     * @param response
     */
    @Override
    public void export(HttpServletResponse response) {
        LocalDate begin = LocalDate.now().plusDays(-30);
        LocalDate end = LocalDate.now().plusDays(-1);
        BusinessDataVO businessDataVO = workspaceService.getBusinessData
                (LocalDateTime.of(begin, LocalTime.MIN), LocalDateTime.of(end, LocalTime.MAX));
        InputStream is = ReportServiceImpl.class.getClassLoader().getResourceAsStream("template\\运营数据报表模板.xlsx");

        try {
            //在内存中新建一个excel表格
            XSSFWorkbook excel = new XSSFWorkbook(is);
            //获取到当前的sheet页
            XSSFSheet sheet = excel.getSheetAt(0);
            //获取第二行
            XSSFRow row = sheet.getRow(1);
            //获取第二个单元格
            row.getCell(1).setCellValue("时间:" + begin + "至" + end);
            //获取第四行
            row = sheet.getRow(3);
            //获取第四行中的第3，5，7个单元格  所对应的索引就是2，4，6
            row.getCell(2).setCellValue(businessDataVO.getTurnover());
            row.getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
            row.getCell(6).setCellValue(businessDataVO.getNewUsers());
            //获取第五行
            row = sheet.getRow(4);
            //获取第五行中的第3个单元格以及第5个单元格，所对应的索引就是2，4
            row.getCell(2).setCellValue(businessDataVO.getValidOrderCount());
            row.getCell(4).setCellValue(businessDataVO.getUnitPrice());
            for (int i = 0; i < 30; i++) {
                LocalDate localDate = begin.plusDays(i);
                businessDataVO = workspaceService.getBusinessData
                        (LocalDateTime.of(localDate, LocalTime.MIN), LocalDateTime.of(localDate, LocalTime.MAX));
                row = sheet.getRow(7 + i);
                row.getCell(1).setCellValue(localDate.toString());
                row.getCell(2).setCellValue(businessDataVO.getTurnover());
                row.getCell(3).setCellValue(businessDataVO.getValidOrderCount());
                row.getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
                row.getCell(5).setCellValue(businessDataVO.getUnitPrice());
                row.getCell(6).setCellValue(businessDataVO.getNewUsers());
            }
            ServletOutputStream os = response.getOutputStream();
            excel.write(os);
            excel.close();
            os.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Integer getByOrderCountAndValidOrderCount(LocalDateTime beginTime, LocalDateTime endTime, Integer status) {
        Map<String, Object> map = new HashMap<>();
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        map.put("status", status);
        //调用mapper方法,实现sql语句 select count(*) from orders where create_time >= ? and create_time <= ?
        return ordersMapper.getByOrderCountAndValidOrderCount(map);
    }


    private Integer getuserCount(LocalDateTime beginTime, LocalDateTime endTime) {
        Map<String, Object> map = new HashMap<>();
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        Integer totalUser = userMapper.getTotalUser(map);
        return totalUser;
    }

}
