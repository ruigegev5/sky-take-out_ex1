package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/shoppingCart")
@Slf4j
@Api(tags = "购物车相关接口")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostMapping("/add")
    @ApiOperation("添加购物车接口")
    public Result add(@RequestBody ShoppingCartDTO shoppingCartDTO){
        //1.接收参数
        log.info("添加购物车参数：{}",shoppingCartDTO);
        //2.调用service层实现业务逻辑处理
        shoppingCartService.addShoppingCart(shoppingCartDTO);
        //3.响应数据给前端
        return Result.success();
    }

    @GetMapping("/list")
    @ApiOperation("查看购物车")
    public Result<List<ShoppingCart>> list(){
        //2.调用service层，实现业务逻辑处理
        List<ShoppingCart>  shoppingCartList= shoppingCartService.list();
        //3.响应数据给前端
        return Result.success(shoppingCartList);
    }

    @DeleteMapping("/clean")
    @ApiOperation("清空购物车")
    public Result clean(){
        //2.调用service层，实现业务逻辑处理
        shoppingCartService.clean();
        //3.响应数据给前端
        return Result.success();
    }
}
