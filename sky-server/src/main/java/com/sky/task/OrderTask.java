package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
public class OrderTask {
    @Autowired
    private OrdersMapper ordersMapper;
//    @Scheduled(cron = "0/5 * * * * ?")
    public void task(){
        //15分钟之后订单自动取消,每一分钟查看一次
        log.info("开始定时任务:{}", LocalDateTime.now());
        //计算要取消的时间
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-15);
        List<Orders> ordersList = ordersMapper.getByStatusAndOrderTime(Orders.PENDING_PAYMENT,localDateTime);
        ordersList.stream().forEach(list->{
            list.setStatus(Orders.CANCELLED);
            list.setCancelTime(LocalDateTime.now());
            list.setRejectionReason("用户太长时间未支付,订单已取消");
            ordersMapper.update(list);
        });
    }

    /**
     * 凌晨一点把未完成的订单修改为已完成
     */
//    @Scheduled(cron = "0 0 1 * * ?")
//    @Scheduled(cron = "5/10 * * * * ?")
    public void noComplete(){
        log.info("把未完成订单修改为已完成:{}",LocalDateTime.now());
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-60);
        List<Orders> list = ordersMapper.getByStatusAndOrderTime(Orders.DELIVERY_IN_PROGRESS, localDateTime);
        list.stream().forEach(orders->{
            orders.setStatus(Orders.COMPLETED);
            ordersMapper.update(orders);
        });
    }
}
