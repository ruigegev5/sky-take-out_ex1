package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.Map;

@Mapper
public interface UserMapper {
    @Select("select * from user where openid=#{openid}")
    public User selectByOpenId(String openid);

    // @Options(useGeneratedKeys = true,keyProperty = "id")
    // @Insert("insert into user(openid, name, phone, sex, id_number, avatar, create_time) VALUES " +
    //         "(#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    void insert(User user);

    /**
     * 根据id查询用户
     * @param id
     * @return
     */
    @Select("select * from user where id = #{id}")
    User getById(Long id);

    /**
     * 用户统计接口
     * @param map
     * @return
     */
    Integer getTotalUser(Map map);
}
